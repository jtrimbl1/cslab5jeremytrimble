package edu.westga.cs1302.bugcensus.model;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 * The Class Bee.
 * 
 * @author CS1302
 * @version Fall 2018
 */
public class Bee extends Insect {
	private BeeCaste caste;

	/**
	 * Instantiates a new bee.
	 * 
	 * @precondition length > 0
	 * @postcondition getLength() == length && getNumberLegs() == 6 && getColor() ==
	 *                Color.BLACK && isWinged() == true && getCaste() == caste
	 * 
	 * @param length
	 *            the length
	 * @param caste
	 *            the caste
	 */
	public Bee(double length, BeeCaste caste) {
		super(length, true, Color.BLACK);
		this.caste = caste;
	}

	/**
	 * Gets the type.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the type
	 */
	public BeeCaste getCaste() {
		return this.caste;
	}

	/**
	 * Sets the type.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param caste
	 *            the new case
	 */
	public void setCaste(BeeCaste caste) {
		this.caste = caste;
	}

	@Override
	public String getDescription() {
		String description = super.getDescription();
		description += System.lineSeparator();
		description += "Caste: " + this.caste;

		return description;
	}

	@Override
	public String toString() {
		return "Bee length=" + this.getLength() + " caste=" + this.caste;
	}

	@Override
	public Canvas getDrawing() {
		Canvas beeDrawing = super.getDrawing();
		GraphicsContext gc = beeDrawing.getGraphicsContext2D();

		gc.setFill(Color.YELLOW);
		gc.fillRect(10, 42, 10, 17);
		gc.fillRect(32, 40, 10, 20);
		gc.fillRect(54, 40, 10, 20);
		gc.fillRect(76, 42, 10, 17);

		return beeDrawing;
	}

	@Override
	public Canvas getDrawing(double xCoor, double yCoor) {
		Canvas canvas = this.getDrawing();

		canvas.setLayoutX(xCoor);
		canvas.setLayoutY(yCoor);
		return canvas;
	}
}
