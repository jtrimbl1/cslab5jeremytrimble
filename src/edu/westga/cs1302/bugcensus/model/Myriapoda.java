package edu.westga.cs1302.bugcensus.model;

import edu.westga.cs1302.bugcensus.resources.UI;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 * The Class Myriapoda.
 * 
 * @author CS1302
 * @version Fall 2018
 */
public class Myriapoda extends Bug {
	private int numberSegments;

	/**
	 * Instantiates a new myriapoda.
	 *
	 * @precondition length > 0 && numberLegs >= 0 && numberSegments > 0 && color !=
	 *               null
	 * @postcondition getLength() == length && getNumberLegs() == numberLegs &&
	 *                getColor() == color && getNumberSegments() == numberSegments
	 * 
	 * @param length
	 *            the length
	 * @param numberLegs
	 *            the number legs
	 * @param numberSegments
	 *            the number segments
	 * @param color
	 *            the color
	 */
	public Myriapoda(double length, int numberLegs, int numberSegments, Color color) {
		super(length, numberLegs, color);
		if (numberSegments <= 0) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NEGATIVE_NUMBER_SEGMENTS);
		}
		this.numberSegments = numberSegments;
	}

	/**
	 * Gets the number of segments.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the number of segments
	 */
	public int getNumberSegments() {
		return this.numberSegments;
	}

	@Override
	public String getDescription() {
		String description = super.getDescription();
		description += System.lineSeparator();
		description += "Number segments: " + this.numberSegments;

		return description;
	}

	@Override
	public String toString() {
		return "Myriapoda length=" + this.getLength() + " #legs=" + this.getNumberLegs() + " #segments="
				+ this.numberSegments + " color=" + this.getColor();
	}

	@Override
	public Canvas getDrawing() {
		double width = this.getLength();
		double height = this.getDrawingHeight();
		Canvas canvas = new Canvas(width, height);
		GraphicsContext gc = canvas.getGraphicsContext2D();
		this.draw(gc);
		return canvas;
	}

	@Override
	public Canvas getDrawing(double xCoord, double yCoord) {
		Canvas canvas = this.getDrawing();

		canvas.setLayoutX(xCoord);
		canvas.setLayoutY(yCoord);
		return canvas;
	}

	@Override
	public double getDrawingHeight() {
		return this.getLength() / 4.0;
	}

	private void draw(GraphicsContext gc) {
		gc.setFill(this.getColor());
		gc.setStroke(this.getColor());

		this.drawBody(gc);
		this.drawLegs(gc);
	}

	private void drawLegs(GraphicsContext gc) {
		int lines = this.getNumberLegs() / 2;
		double incrementLength = this.getLength() / (lines + 1);
		double xValue = incrementLength;

		for (int i = 0; i < lines; i++) {
			gc.strokeLine(xValue, 0, xValue, this.getDrawingHeight());
			xValue += incrementLength;
		}
	}

	private void drawBody(GraphicsContext gc) {
		double segHeight = this.getDrawingHeight() / 2;
		double segWidth = this.getLength() / this.numberSegments;
		double segStartX = 0;
		double segStartY = this.getDrawingHeight() / 4;

		for (int i = 0; i <= this.numberSegments; i++) {
			gc.fillOval(segStartX, segStartY, segWidth, segHeight);
			segStartX += segWidth;
		}
	}

}
