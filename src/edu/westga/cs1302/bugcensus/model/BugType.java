package edu.westga.cs1302.bugcensus.model;

import edu.westga.cs1302.bugcensus.resources.UI;

/**
 * The Enum BugType.
 * 
 * @author CS1302
 * @version Fall 2018
 */
public enum BugType {
	INSECT, BEE, MYRIAPODA;
	
	/**
	 * Parses the bug type.
	 *
	 * @precondition type != null && BugType.values() contains type
	 * @postcondition none
	 * 
	 * @param type the type to be parsed
	 * @return the bug type as type BugType
	 */
	public static BugType parseType(String type) {
		if (type == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_BUGTYPE);
		}
		type = type.toUpperCase();
		switch (type) {
			case "INSECT":
				return INSECT;
			case "BEE":
				return BEE;
			case "MYRIAPODA":
				return MYRIAPODA;
			default:
				throw new IllegalArgumentException("Invalid caste");
		}
	}
	
	/**
	 * Determines the bug type.
	 *
	 * @precondition bug != null
	 * @postcondition none
	 * 
	 * @param bug the bug object whose type is determined
	 * @return the bug type 
	 */
	public static BugType getType(Bug bug) {
		if (bug == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_BUG);
		}
		if (bug instanceof Bee) {
			return BEE;
		} else if (bug instanceof Insect) {
			return INSECT;
		} else if (bug instanceof Myriapoda) {
			return MYRIAPODA;
		}
		throw new IllegalArgumentException("Invalid object type");
	}
}
