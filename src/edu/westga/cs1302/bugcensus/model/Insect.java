package edu.westga.cs1302.bugcensus.model;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 * The Class Insect.
 * 
 * @author CS1302
 * @version Fall 2018
 */
public class Insect extends Bug {
	private static final double DEFAULT_CANVAS_WIDTH = 100;
	private static final double DEFAULT_CANVAS_HEIGHT = 100;

	private boolean winged;

	/**
	 * Instantiates a new insect.
	 *
	 * @precondition length > 0 and color != null
	 * @postcondition getLength() == length && getNumberLegs() == 6 && getColor() ==
	 *                color && isWinged() == winged
	 * 
	 * @param length
	 *            the length
	 * @param winged
	 *            the winged
	 * @param color
	 *            the color
	 */
	public Insect(double length, boolean winged, Color color) {
		super(length, 6, color);
		this.winged = winged;
	}

	/**
	 * Checks if is winged.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return true, if is winged
	 */
	public boolean isWinged() {
		return this.winged;
	}

	/**
	 * Sets the winged.
	 *
	 * @precondition none
	 * @postcondition getWinged() == winged
	 * 
	 * @param winged
	 *            the new winged
	 */
	public void setWinged(boolean winged) {
		this.winged = winged;
	}

	@Override
	public Canvas getDrawing() {
		double width = this.getLength();
		double height = this.getLength();
		Canvas canvas = new Canvas(width, height);
		GraphicsContext gc = canvas.getGraphicsContext2D();
		double scaleX = width / DEFAULT_CANVAS_WIDTH;
		double scaleY = height / DEFAULT_CANVAS_HEIGHT;
		gc.scale(scaleX, scaleY);
		this.draw(gc);
		return canvas;
	}

	@Override
	public Canvas getDrawing(double xCoor, double yCoor) {
		Canvas canvas = this.getDrawing();

		// set the relative location of the canvas
		canvas.setLayoutX(xCoor);
		canvas.setLayoutY(yCoor);
		return canvas;
	}

	/**
	 * Draws the insect using the default dimension
	 * 
	 * @param gc
	 *            he GraphicsContext to be drawn on
	 */
	private void draw(GraphicsContext gc) {
		gc.setFill(this.getColor());
		gc.setStroke(this.getColor());

		// draw body
		gc.fillOval(0, 40, 100, 20);

		// draw legs
		gc.strokeLine(5, 5, 50, 50);
		gc.strokeLine(5, 95, 50, 50);
		gc.strokeLine(55, 0, 50, 50);
		gc.strokeLine(55, 100, 50, 50);
		gc.strokeLine(90, 5, 50, 50);
		gc.strokeLine(90, 95, 50, 50);

		if (this.isWinged()) {
			gc.strokeOval(40, 0, 20, 45);
			gc.strokeOval(40, 55, 20, 45);
		}
	}

	@Override
	public String toString() {
		return "Insect length=" + this.getLength() + " color=" + this.getColor() + " winged=" + this.winged;
	}
}
