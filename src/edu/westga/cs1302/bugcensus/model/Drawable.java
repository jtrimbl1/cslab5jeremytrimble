package edu.westga.cs1302.bugcensus.model;

import javafx.scene.canvas.Canvas;

/**
 * The Drawable interface
 * 
 * @author jeremy.trimble
 * @version 10/9/2018
 */
public interface Drawable {

	/**
	 * Returns a canvas with a drawing of the object at coordinates 0,0.
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return a canvas of the drawing.
	 */
	Canvas getDrawing();

	/**
	 * Returns a canvas with a drawing of the object at the specified coordinates
	 * 
	 * @prescondition none
	 * @postcondition none
	 * 
	 * @param xCoord
	 *            the x coordinate the canvas is to start on
	 * 
	 * @param yCoord
	 *            the y coordinate the canvas is to start on
	 * @return a canvas of the drawing
	 */
	Canvas getDrawing(double xCoord, double yCoord);

	/**
	 * Return the width of the canvas
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the width of the canvas
	 */
	double getDrawingWidth();

	/**
	 * Returns the height of the canvas
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the height of the canvas
	 */
	double getDrawingHeight();
}
